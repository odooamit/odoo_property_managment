# -*- coding: utf-8 -*-
##############################################################################
#
# 	OpenERP, Open Source Management Solution
# 	Copyright (C) 2011-Today Serpent Consulting Services PVT LTD (<http://www.serpentcs.com>)
#
# 	This program is free software: you can redistribute it and/or modify
# 	it under the terms of the GNU Affero General Public License as
# 	published by the Free Software Foundation, either version 3 of the
# 	License, or (at your option) any later version.
#
# 	This program is distributed in the hope that it will be useful,
# 	but WITHOUT ANY WARRANTY; without even the implied warranty of
# 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# 	GNU Affero General Public License for more details.
#
# 	You should have received a copy of the GNU Affero General Public License
# 	along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################

from openerp.osv import osv, fields


class account_voucher(osv.Model):
	_inherit = "account.voucher"

	_columns = {
		'tenancy_id':fields.many2one('account.analytic.account', 'Tenancy'),
		'property_id' : fields.many2one('account.asset.asset', 'Property')
	}

	def first_move_line_get(self, cr, uid, voucher_id, move_id, company_currency, current_currency, context=None):
		'''
			This Method is will Return a dictionary to be use to create
			the first account move line of given voucher.

		:@param self: The object pointer
		:@param cr: the current row, from the database cursor,
		:@param uid: the current user’s ID for security checks,
		:param voucher_id: Id of voucher for what we are creating account_move.
		:param move_id: Id of account move where this line will be added.
		:param company_currency: id of currency of the company to which the voucher belong
		:param current_currency: id of currency of the voucher
		:return: mapping between field name and value of account move line to create
		:return type: dictionary
		'''
		res = super(account_voucher, self).first_move_line_get(cr, uid, voucher_id, move_id, company_currency, current_currency, context=context)
		if voucher_id:
			voucher_brw = self.browse(cr, uid, voucher_id, context=context)
			res.update({
				'asset_id':voucher_brw.property_id and voucher_brw.property_id.id or False,
				'new_tenancy_id':voucher_brw.tenancy_id and voucher_brw.tenancy_id.id or False
			})
		return res

	def back_to_tenancy(self, cr, uid, ids, context=None):
		"""
			This method will open a Tenancy form view.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		"""
		for vou_brw in self.browse(cr, uid, ids, context=context):
			open_move_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'property_management', 'property_analytic_view_form')[1]
			tenancy_id = vou_brw.tenancy_id and vou_brw.tenancy_id.id or False
			if tenancy_id:
				return {
					'view_type': 'form',
					'view_id': open_move_id,
					'view_mode': 'form',
					'res_model': 'account.analytic.account',
					'res_id':tenancy_id,
					'type': 'ir.actions.act_window',
					'target': 'current',
					'context': context,
				}
			return True


class account_voucher_line(osv.Model):
	_inherit = "account.voucher.line"

	_columns = {
		'tenancy_rent_id':fields.many2one('tenancy.rent.schedule', 'Tenancy Rent'),
		'property_id' : fields.many2one('account.asset.asset', 'Property')
	}

	def onchange_tenant_rent(self, cr, uid, id, tenancy_rent_id, context=None):
		"""
			when you change value of tenancy_rent_id field , this method will change
			value of amount field accordingly.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param id: List of IDs
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		rent_amount = 0.00 
		tenancy_rent_brw = self.pool.get('tenancy.rent.schedule').browse(cr, uid, tenancy_rent_id, context=context)
		return {'value':{'amount':tenancy_rent_brw.amount}}


class account_move_line(osv.Model):
	_inherit = "account.move.line"
	_columns = {
		'property_id':fields.many2one('account.asset.asset', 'Property'),
		'new_tenancy_id':fields.many2one('account.analytic.account', 'Tenancy'),
	}
