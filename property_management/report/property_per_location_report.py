# -*- coding: utf-8 -*-
##############################################################################
#
#	OpenERP, Open Source Management Solution
#	Copyright (C) 2011-Today Serpent Consulting Services PVT LTD (<http://www.serpentcs.com>)
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU Affero General Public License as
#	published by the Free Software Foundation, either version 3 of the
#	License, or (at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU Affero General Public License for more details.
#
#	You should have received a copy of the GNU Affero General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################

from openerp.report import report_sxw
from openerp.osv import osv


class property_per_location_report(report_sxw.rml_parse):

	def __init__(self, cr, uid, name, context=None):
		super(property_per_location_report, self).__init__(cr, uid, name, context=context)
		self.localcontext.update({
			'property_location' : self.property_location,
		})


	def property_location(self,data):
		property_list = []
		sub_property_list = []
		property_obj = self.pool.get('account.asset.asset')
		property_ids = property_obj.search(self.cr, self.uid, [('state_id', '=', data['state_id'][0])])
		if property_ids:
			for property_data in property_obj.browse(self.cr, self.uid, property_ids):
				if property_data.child_ids and property_data.child_ids.ids:
					for sub in property_data.child_ids:
						sub_property_list.append(sub.id)
						property_dict = {
								'name':property_data.name,
								'child_ids':sub.name,
								'city':sub.city,
								'state_id':property_data.state_id.name,
								'township':sub.township
								}
						property_list.append(property_dict)
				else:
					if property_data.id not in sub_property_list:
						property_dict = {
								'name':property_data.name,
								'child_ids':False,
								'city':property_data.city,
								'state_id':property_data.state_id.name,
								'township':property_data.township
								}
						property_list.append(property_dict)
		return property_list

class report_property_per_location(osv.AbstractModel):

	_name = "report.property_management.report_property_per_location1"
	_inherit = "report.abstract_report"
	_template = "property_management.report_property_per_location1"
	_wrapped_report_class = property_per_location_report
