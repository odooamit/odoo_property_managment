# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2011-Today Serpent Consulting Services PVT LTD (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################

import pending_rent
import tenancy_detail
import property_room_report
import tenancy_detail_by_tenant
import contract_expiry
import safety_certificate_expiry
import income_expenditure
import document_expiry
import property_per_location_report
import gfa_report
import operational_cost_report
import investment_report
import occupancy_performance_report
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: